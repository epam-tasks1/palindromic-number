﻿using System;

namespace PalindromicNumberTask
{
    public static class NumbersExtension
    {
        public static bool IsPalindromicNumber(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("number is less than zero.");
            }

            int logarithm = (int)Math.Log10(number);

            if (number / 10 == 0)
            {
                return true;
            }

            int power = (int)Math.Pow(10, logarithm);

            int leftSide = number / power;
            int rightSide = number % 10;

            if (leftSide == rightSide)
            {
                return IsPalindromicNumber((number - (leftSide * power)) / 10);
            }

            return false;
        }
    }
}
